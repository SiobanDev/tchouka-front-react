import React, { FC } from 'react';
//Libraries
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFrownOpen, faSmile } from '@fortawesome/free-regular-svg-icons';
import { NotificationType } from '../../models/NotificationInterfaces';

interface ResponseIconProps {
  severityKind: NotificationType;
}

const ResponseIcon: FC<ResponseIconProps> = ({ severityKind }) => {
  return severityKind === 'success' ? (
    <FontAwesomeIcon className="modal-smiley" icon={faSmile} />
  ) : (
    <FontAwesomeIcon className="modal-smiley" icon={faFrownOpen} />
  );
};

export default ResponseIcon;
