import { CompositionItem } from '../../../models/ClientDataInterfaces';

export function getWholeMovememntDurationList(composition: CompositionItem[]): number[] {
  const allMovememntDurationList: number[] = [];

  composition.forEach((compositionItem) => {
    const wholeMovementDuration = compositionItem.durationList.reduce((a, b) => a + b);
    allMovememntDurationList.push(wholeMovementDuration);
  });

  return allMovememntDurationList;
}

export function getAllMovementDelayList(allMovementDurationList: number[]): number[] {
  const allMovementDelayList = [0];

  const addSumOfImageDelay = (previousSum: number, nextDuration: number): number => {
    allMovementDelayList.push(previousSum + nextDuration);
    return previousSum + nextDuration;
  };

  allMovementDurationList.reduce(addSumOfImageDelay, 0);

  return allMovementDelayList;
}
