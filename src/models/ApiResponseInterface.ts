export interface ApiResponseSuccess<T> {
  success: true;
  data: T;
  message: string;
}

export interface ApiResponseFail {
  success: false;
  message: string;
}

export type ApiResponse<T> = ApiResponseSuccess<T> | ApiResponseFail;
