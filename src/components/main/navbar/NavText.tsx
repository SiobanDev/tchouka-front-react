import React, { FC, useContext } from 'react';
//Libraries
import { Link } from 'react-router-dom';
//Contexts
import ScoreContext from '../../../context/ScoreContext';
import CompositionContext from '../../../context/CompositionContext';
import StepContext from '../../../context/StepContext';
//Utils
import { adaptComposition } from '../../step2/Step2.utils';
import { removeStoredComposition, storeComposition, storeScore } from '../../../services/localStorageService';

interface NavTextProps {
  isAllowed: boolean;
  urlMenuList: string[];
  itemNameMenuList: string[];
  i: number;
}

const NavText: FC<NavTextProps> = ({ isAllowed, urlMenuList, itemNameMenuList, i }) => {
  const { score } = useContext(ScoreContext);
  const { composition, setComposition } = useContext(CompositionContext);
  const { currentStep, setCurrentStep } = useContext(StepContext);

  if (!isAllowed) {
    return (
      <li id={`navbar-step${i}`} className="navbar-element" key={i}>
        <div className="navbar-link">{itemNameMenuList[i]}</div>
      </li>
    );
  }

  return (
    <li id={`navbar-step${i}`} className="navbar-element" key={i}>
      <Link
        className="navbar-link"
        to={urlMenuList[i]}
        onClick={() => {
          if (currentStep === 1 && i === 2) {
            storeScore(score);
          } else if ((currentStep === 2 || currentStep === 3) && i === 1) {
            removeStoredComposition();
            setComposition([]);
          } else if (currentStep === 2 && i === 3) {
            const adaptedComposition = adaptComposition(composition);
            setComposition(adaptedComposition);
            storeComposition(adaptedComposition);
          }

          setCurrentStep(i);
        }}
      >
        {itemNameMenuList[i]}
      </Link>
    </li>
  );
};

export default NavText;
