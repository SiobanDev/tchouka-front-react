import React from 'react';
//Models
import { ScoreItem } from '../models/ClientDataInterfaces';

interface InitialContextValuesProps {
  score: ScoreItem[];
  setScore: (score: ScoreItem[]) => void;
  freeTime: number;
  allNotesWidth: number;
  setAllNotesWidth: (notesWidth: number) => void;
  addedNoteWidth: number;
}

const initialContextValues: InitialContextValuesProps = {
  score: [],
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setScore: () => {},
  freeTime: 42,
  allNotesWidth: 0,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setAllNotesWidth: () => {},
  addedNoteWidth: 5,
};

const ScoreContext = React.createContext(initialContextValues);

export default ScoreContext;
