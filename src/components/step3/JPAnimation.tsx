import React, { FC, useContext, useEffect } from 'react';
//Styles
import './Step3.style.scss';
//Contexts
import AnimationContext from '../../context/AnimationContext';
import CompositionContext from '../../context/CompositionContext';
//Utils
import {
  addBackgroundToImages,
  getAllImageSrcList,
  getAllSoundDelayList,
  getAllSoundDurationList,
} from './Step3.utils';
//Constants
import { jpNeutre } from '../../config/mediasConstants';
import { getStoredComposition } from '../../services/localStorageService';

interface JPAnimationProps {
  allImageDelayList: number[];
}

export const JPAnimation: FC<JPAnimationProps> = ({ allImageDelayList }) => {
  const { composition, setComposition } = useContext(CompositionContext);
  const { playingAnimation, timeCode, lastSoundCount, setLastSoundCount } = useContext(AnimationContext);
  const allImageSrcList = getAllImageSrcList(composition);
  const allSoundDurationList = getAllSoundDurationList(composition);
  const allSoundDelayList = getAllSoundDelayList(allSoundDurationList);

  useEffect(() => {
    if (composition.length === 0) {
      const stored = getStoredComposition();
      if (stored) {
        setComposition(stored);
      }
    }
  }, [composition.length, setComposition]);

  const soundCount = (() => {
    const soundCountTmp = allSoundDelayList.findIndex((movementDelay) => movementDelay > timeCode) - 1;

    return soundCountTmp >= 0 && soundCountTmp < allSoundDelayList.length - 2 ? soundCountTmp : composition.length - 1;
  })();

  const imageCount = (() => {
    const imageCountTmp = allImageDelayList.findIndex((movementDelay) => movementDelay > timeCode) - 1;

    return imageCountTmp >= 0 ? imageCountTmp : allImageDelayList.length - 1;
  })();

  useEffect(() => {
    if (playingAnimation && soundCount !== lastSoundCount) {
      const movementSound = new Audio();
      movementSound.src = composition[soundCount].sound;
      movementSound.play().catch((e) => {
        console.error('Error while playing sound', e);
      });

      setLastSoundCount(soundCount);
    }
  }, [composition, lastSoundCount, playingAnimation, setLastSoundCount, soundCount]);

  if (playingAnimation && allImageSrcList.length > 0) {
    return (
      <img className="movement-image" src={addBackgroundToImages(allImageSrcList, imageCount)} alt="movement-img" />
    );
  }
  return <img className="movement-image" src={jpNeutre} alt="movement-img" />;
};

export default JPAnimation;
