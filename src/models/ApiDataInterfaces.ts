//Models
import { CompositionItem, ScoreItem } from './ClientDataInterfaces';

export interface UserComposition {
  id: number;
  user: number;
  title: string;
  movementList: CompositionItem[];
}
export type CreateUserComposition = Omit<UserComposition, 'id'>;

export interface UserScore {
  id: number;
  user: number;
  title: string;
  noteList: ScoreItem[];
}
export type CreateUserScore = Omit<UserScore, 'id'>;

export interface AuthData {
  username: string;
  password: string;
}

export interface LoggedResponse {
  token: string;
  addedData: {
    id: number;
  };
}
