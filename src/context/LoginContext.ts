import React from 'react';

interface InitialContextValuesProps {
  loggedIn: boolean;
  setLoggedIn: (loggedIn: boolean) => void;
}

const initialContextValues: InitialContextValuesProps = {
  loggedIn: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setLoggedIn: () => {},
};

const LoginContext = React.createContext(initialContextValues);

export default LoginContext;
