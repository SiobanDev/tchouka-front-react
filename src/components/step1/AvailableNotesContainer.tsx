import React, { FC, useContext } from 'react';
//Contexts
import ScoreContext from '../../context/ScoreContext';
//Components
import AvailableNote from './AvailableNote';
//Styles
import './AvailableNotesContainer.style.scss';
//Data
import { defaultAvailableNotes } from '../../data/defaultNotes';
import { removeStoredScore } from '../../services/localStorageService';
import { ScoreItem } from '../../models/ClientDataInterfaces';

export const AvailableNotesContainer: FC = () => {
  const { score, setScore, allNotesWidth, setAllNotesWidth, addedNoteWidth } = useContext(ScoreContext);

  return (
    <div id="notes-to-choose-container">
      {defaultAvailableNotes.map((note) => (
        <div className="available-note-container" key={note.id}>
          <AvailableNote
            imageSource={note.imageSrc}
            onClick={() => {
              removeStoredScore();

              const newScoreNote: ScoreItem = {
                id: score.length,
                duration: note.duration,
                imageSrc: note.imageSrc,
              };

              setScore([...score, newScoreNote]);
              setAllNotesWidth(allNotesWidth + addedNoteWidth);
            }}
          />
          <p className="note-caption">{note.caption}</p>
        </div>
      ))}
    </div>
  );
};

export default AvailableNotesContainer;
