import React, { FC } from 'react';
import { contactEmail } from '../config/mainConstants';
//styles
import './Footer.style.scss';

const Footer: FC = () => {
  return (
    <footer>
      <a href="/credits">Crédits</a>
      <a href="/mon-avis">Donner mon avis</a>
      {contactEmail ? <a href={`mailto:${contactEmail}`}>Contact</a> : null}
    </footer>
  );
};

export default Footer;
