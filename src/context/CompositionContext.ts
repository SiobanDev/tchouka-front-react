import React from 'react';
//Models
import { CompositionItem } from '../models/ClientDataInterfaces';

interface InitialContextValuesProps {
  composition: CompositionItem[];
  setComposition: (composition: CompositionItem[]) => void;
  isLastItemRemoved: boolean;
  setIsLastItemRemoved: (isLastItemRemoved: boolean) => void;
}

const initialContextValues: InitialContextValuesProps = {
  composition: [],
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setComposition: () => {},
  isLastItemRemoved: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setIsLastItemRemoved: () => {},
};

const CompositionContext = React.createContext(initialContextValues);

export default CompositionContext;
