import React, { FC, useContext, useEffect, useRef, useState } from 'react';
//Styles
import './Controls.style.scss';
//Contexts
import AnimationContext from '../../../context/AnimationContext';
//Constants
import { timeCodeInterval } from '../../../config/mainConstants';
//Components
import TimeLine from './Timeline';
//Libraries
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowCircleLeft,
  faArrowCircleRight,
  faPause,
  faPlay,
  faStepBackward,
  faStop,
} from '@fortawesome/free-solid-svg-icons';

interface ControlsProps {
  allImageDelayList: number[];
}

const Controls: FC<ControlsProps> = ({ allImageDelayList }) => {
  const {
    playingAnimation,
    setPlayingAnimation,
    timeCode,
    setTimeCode,
    repeat,
    setRepeat,
    setLastSoundCount,
  } = useContext(AnimationContext);
  const handleCheckboxChange = (): void => {
    setRepeat(!repeat);
  };
  const onClickMovingStep = 500;
  const [forwardingValue, setForwardingValue] = useState(0);
  const timelineRef = useRef<HTMLDivElement>(null);
  const cursorProgress = (timeCode / allImageDelayList[allImageDelayList.length - 1]) * 100;

  const timelineStyleWidthValue = timelineRef.current?.clientWidth ?? 10000;
  const maxShiftOfChronologyContent = timelineStyleWidthValue - onClickMovingStep;

  const movingTimelinePosition = (cursorProgress / 100) * timelineStyleWidthValue;

  const timelineStyleLeft = playingAnimation
    ? Math.min(movingTimelinePosition, maxShiftOfChronologyContent)
    : forwardingValue;

  const timelineStyle = {
    transition: `left 500ms ease-out`,
    left: `${-timelineStyleLeft}px`,
    width: `calc(15% * ${allImageDelayList[allImageDelayList.length - 1] / onClickMovingStep})`,
  };

  const setClampedForwardingValue = (value: number): void => {
    setForwardingValue(Math.max(0, Math.min(maxShiftOfChronologyContent, value)));
  };

  const resetAnimation = (): void => {
    setForwardingValue(0);
    setPlayingAnimation(false);
    setTimeCode(0);
    setLastSoundCount(-1);
  };

  const stopAnimation = (): void => {
    setForwardingValue(timelineStyleLeft);
    setPlayingAnimation(false);
    setTimeCode(allImageDelayList[allImageDelayList.length - 1]);
    setLastSoundCount(-1);
  };

  useEffect(() => {
    if (playingAnimation && timeCode >= allImageDelayList[allImageDelayList.length - 1]) {
      setForwardingValue(timelineStyleLeft);
      setPlayingAnimation(false);
    }

    if (playingAnimation) {
      if (
        timeCode === allImageDelayList[allImageDelayList.length - 1] ||
        timeCode >= allImageDelayList[allImageDelayList.length - 1]
      ) {
        setPlayingAnimation(false);
        setLastSoundCount(-1);

        if (repeat) {
          setTimeCode(0);
          setPlayingAnimation(true);
        }
      } else if (timeCode < allImageDelayList[allImageDelayList.length - 1]) {
        const start = Date.now();
        const timer: NodeJS.Timeout = setInterval(() => setTimeCode(timeCode + Date.now() - start), timeCodeInterval);

        return () => {
          clearInterval(timer);
        };
      }
    }
  }, [
    allImageDelayList,
    playingAnimation,
    repeat,
    setLastSoundCount,
    setPlayingAnimation,
    setTimeCode,
    timeCode,
    timelineStyleLeft,
  ]);

  return (
    <div id="step3-chronology">
      <div id="chronology-container">
        {!playingAnimation && timelineStyleLeft > 0 ? (
          <div id="go-backward-container">
            <FontAwesomeIcon
              className="round-icon"
              icon={faArrowCircleLeft}
              onClick={() => {
                setClampedForwardingValue(timelineStyleLeft - onClickMovingStep);
              }}
            />
          </div>
        ) : null}
        {!playingAnimation && timelineStyleLeft < maxShiftOfChronologyContent ? (
          <div id="go-forward-container">
            <FontAwesomeIcon
              className="round-icon"
              icon={faArrowCircleRight}
              onClick={() => {
                setClampedForwardingValue(timelineStyleLeft + onClickMovingStep);
              }}
            />
          </div>
        ) : null}

        <div id="timeline-container" ref={timelineRef} style={timelineStyle}>
          <TimeLine allImageDelayList={allImageDelayList} cursorProgress={cursorProgress} />
        </div>
      </div>

      <div className="step3-commands-container">
        <div className="round-icon play-item">
          <FontAwesomeIcon icon={faStepBackward} onClick={resetAnimation} />
        </div>
        <div className="round-icon play-item">
          <FontAwesomeIcon
            icon={faPlay}
            onClick={() => {
              setPlayingAnimation(true);
            }}
          />
        </div>
        <div className="round-icon play-item">
          <FontAwesomeIcon
            icon={faPause}
            onClick={() => {
              setForwardingValue(timelineStyleLeft);
              setPlayingAnimation(false);
            }}
          />
        </div>
        <div className="round-icon play-item">
          <FontAwesomeIcon icon={faStop} onClick={stopAnimation} />
        </div>
        <form id="repeat-item">
          <input
            type="checkbox"
            id="repeat-option"
            name="repeat-option"
            checked={repeat}
            onChange={handleCheckboxChange}
          />
          <p>Répéter</p>
        </form>
      </div>
    </div>
  );
};

export default Controls;
