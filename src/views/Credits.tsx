import React, { FC } from 'react';
import { contactEmail } from '../config/mainConstants';

const Credits: FC = () => {
  return (
    <section className="main-content">
      <h3>CRÉDITS</h3>
      <p>Tous les éléments graphiques du site ainsi que le design général sont la propriété exclusive de Sioban.</p>
      {contactEmail ? (
        <p>
          Pour la contacter, suivez{' '}
          <a className="inline-link" href={`mailto:${contactEmail}`}>
            ce lien
          </a>
          .
        </p>
      ) : null}
      <p>
        Les icônes sont sous la{' '}
        <a className="inline-link" href="https://fontawesome.com/license">
          licence de Font Awesome
        </a>
        .
      </p>
    </section>
  );
};

export default Credits;
