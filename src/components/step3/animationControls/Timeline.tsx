import React, { FC, useContext } from 'react';
//Styles
import './Timeline.style.scss';
import scssConstants from './TimelineConstants.module.scss';
//Contexts
import AnimationContext from '../../../context/AnimationContext';
import CompositionContext from '../../../context/CompositionContext';
//Constants
import { timeCodeInterval } from '../../../config/mainConstants';
//Utils
import { getAllMovementDelayList, getWholeMovememntDurationList } from './Timeline.utils';

interface TimeLineProps {
  allImageDelayList: number[];
  cursorProgress: number;
}

const TimeLine: FC<TimeLineProps> = ({ allImageDelayList, cursorProgress }) => {
  const { composition } = useContext(CompositionContext);
  const { playingAnimation } = useContext(AnimationContext);
  const wholeMovementDurationList = getWholeMovememntDurationList(composition);
  const wholeMovementDelayList = getAllMovementDelayList(wholeMovementDurationList);
  const numberOfSecondsInAnimation = Math.floor(allImageDelayList[allImageDelayList.length - 1] / 1000);
  const timeSecondElementList = [];
  const cursorPosition = cursorProgress > 100 ? 100 : cursorProgress;
  const cursorStyle = {
    transition: playingAnimation ? `left ${timeCodeInterval}ms linear` : undefined,
    left: `calc(${cursorPosition}% - ${scssConstants.halfCursorWidth}px + ${scssConstants.halfLineWidth}px)`,
  };

  for (let i = 0; i <= numberOfSecondsInAnimation; i++) {
    const percentageElementPosition = (1000 * 100) / wholeMovementDelayList[wholeMovementDelayList.length - 1];

    timeSecondElementList.push(
      <div
        className="time-second-landmark"
        key={`second-landmark-${i}`}
        style={{ left: `${i * percentageElementPosition}%` }}
      ></div>
    );
  }

  const landmarkPositionList: number[] = [];

  //The last item of the wholeMovementDelayList is the whole duration of the animation but does not correspond to any movement
  for (const delay of wholeMovementDelayList) {
    const landmarkPosition = (delay * 100) / wholeMovementDelayList[wholeMovementDelayList.length - 1];
    landmarkPositionList.push(landmarkPosition);
  }

  return (
    <>
      <div id="timeline-cursor" style={cursorStyle}></div>
      {composition.map((movement, i) => {
        const imageSrcWithFocus = movement.movementList[0].replace('.svg', '-rd.svg');

        return (
          <img
            className="movement-image-thumbnail"
            src={imageSrcWithFocus}
            key={movement.id}
            style={{
              left: `calc(${landmarkPositionList[i]}% - ${scssConstants.halfThumbnailWidth}px + ${scssConstants.halfLineWidth}px)`,
            }}
            alt="movement-img-thumbnail"
          />
        );
      })}
      <div id="timeline-chronology"></div>
      {timeSecondElementList}
      {composition.map((movement, i) => (
        <p
          className="singingWord"
          key={movement.id}
          style={{
            left: `calc(${landmarkPositionList[i]}% - ${scssConstants.halfLandmarkWidth}px)`,
          }}
        >
          {movement.singingWord}
        </p>
      ))}
      {landmarkPositionList.map((landmarkPosition, i) => (
        <div
          id={`movement-landmark-${i}`}
          className="movement-landmark"
          style={{
            left: `calc(${landmarkPosition}% - ${scssConstants.halfLandmarkWidth}px + ${scssConstants.halfLineWidth}px)`,
          }}
          key={landmarkPosition}
        ></div>
      ))}
    </>
  );
};

export default TimeLine;
