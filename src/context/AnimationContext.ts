import React from 'react';

interface InitialContextValuesProps {
  playingAnimation: boolean;
  setPlayingAnimation: (playing: boolean) => void;
  timeCode: number;
  setTimeCode: (timeCode: number) => void;
  repeat: boolean;
  setRepeat: (repeat: boolean) => void;
  lastSoundCount: number;
  setLastSoundCount: (lastSoundCount: number) => void;
}

const initialContextValues: InitialContextValuesProps = {
  playingAnimation: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setPlayingAnimation: () => {},
  timeCode: 0,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setTimeCode: () => {},
  repeat: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setRepeat: () => {},
  lastSoundCount: -1,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setLastSoundCount: () => {},
};

const AnimationContext = React.createContext(initialContextValues);

export default AnimationContext;
