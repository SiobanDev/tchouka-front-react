import { BodyPartWord } from '../config/mediasConstants';

export interface ScoreItem {
  id: number;
  duration: number;
  imageSrc: string;
}

export interface CompositionItem {
  id: number;
  durationList: number[];
  movementList: string[];
  singingWord: BodyPartWord;
  sound: string;
}

export interface NoteDefinition {
  id: number;
  duration: number;
  imageSrc: string;
  caption: string;
}
