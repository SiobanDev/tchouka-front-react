import React from 'react';

interface InitialContextValuesProps {
  currentStep: number;
  setCurrentStep: (step: number) => void;
}

const initialContextValues: InitialContextValuesProps = {
  currentStep: 0,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setCurrentStep: () => {},
};

const StepContext = React.createContext(initialContextValues);

export default StepContext;
