//TODO add quarter beat
// export const quarterBeatDuration = 250;
export const halfBeatDuration = 125;
export const oneBeatDuration = 250;
export const TwoBeatsDuration = 500;
export const FourBeatsDuration = 1000;

const audioPath = '/media/audio/';
const imagePath = '/media/images/';
const movementPath = '/media/images/movements/';
// const notePath = "/media/images/notes/";

export const redStave = imagePath + 'red-stave.svg';
export const blueStave = imagePath + 'blue-stave.svg';
// export const quarterBeatImage = notePath + "quarter-beat.svg";
// export const halfBeatImage = notePath + "half-beat.svg";
// export const oneBeatImage = notePath + "one-beat.svg";
// export const TwoBeatsImage = notePath + "two-beats.svg";
// export const FourBeatsImage = notePath + "four-beats.svg";

export const bodyPartList = ['tchou', 'tchi', 'cla', 'bou', 'bi', 'dou', 'di', 'pou', 'pi'] as const;
export type BodyPartWord = typeof bodyPartList[number];

export const jpNeutre = movementPath + 'position-neutre.svg';
export const jpNeutreTransp = movementPath + 'position-neutre-transp.svg';

export function bodyPartMovementList(bodyPart: BodyPartWord): string[] {
  switch (bodyPart) {
    case 'bi':
    case 'bou':
    case 'cla':
    case 'di':
    case 'dou':
      return [movementPath + bodyPart + '.svg'];
    case 'pi':
    case 'pou':
    case 'tchi':
    case 'tchou':
      return [movementPath + bodyPart + '-debut.svg', movementPath + bodyPart + '-fin.svg'];
  }
}

export function bodyPartSound(bodyPart: BodyPartWord): string {
  return audioPath + bodyPart + '.mp3';
}
