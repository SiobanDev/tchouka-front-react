import React, { FC } from 'react';

interface AvailableNoteProps {
  imageSource: string;
  onClick: () => void;
}

const AvailableNote: FC<AvailableNoteProps> = ({ imageSource, onClick }) => {
  return (
    <div onClick={onClick}>
      <img className="note-to-choose" src={imageSource} alt="musical-note" />
    </div>
  );
};

export default AvailableNote;
