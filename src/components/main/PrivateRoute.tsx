import React, { FC } from 'react';
//Libraries
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { getStoredToken } from '../../services/localStorageService';

interface PrivateRouteProps extends RouteProps {
  // Remove undefined from component type
  // Did not manage to type it correctly, thus:
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: React.ComponentType<any>;
}

const PrivateRoute: FC<PrivateRouteProps> = ({ component: Component, ...rest }) => {
  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page
    <Route {...rest} render={(props) => (getStoredToken() ? <Component {...props} /> : <Redirect to="/connexion" />)} />
  );
};

export default PrivateRoute;
