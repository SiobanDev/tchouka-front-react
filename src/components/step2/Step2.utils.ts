//Constants
import { jpNeutre } from '../../config/mediasConstants';
//Data
import { defaultAvailableNotes } from '../../data/defaultNotes';
import { CompositionItem } from '../../models/ClientDataInterfaces';

export function adaptComposition(composition: CompositionItem[]): CompositionItem[] {
  const compositionTmp = [...composition];

  for (const compoItem of compositionTmp) {
    //Add neutral position between two same consecutive movements
    const movementListTmp = [...compoItem.movementList, jpNeutre];

    compoItem.movementList = movementListTmp;

    //Recount the duration for each image
    const wholeMovementDuration = compoItem.durationList.reduce((a, b) => a + b);

    //We have just added the neutral position at the end of each movement and now we want the duration of this image is pretty short (see neutralPositionDuration) but the whole duration of the movement may not change et mostly the other images must share the same duration
    const shorterNoteDuration = defaultAvailableNotes[defaultAvailableNotes.length - 1].duration;
    const imageDurationTmp = shorterNoteDuration / 2 - 50;

    const numberOfImagesExceptNeutral = compoItem.movementList.length - 1;
    const neutralPositionDuration = wholeMovementDuration - imageDurationTmp * numberOfImagesExceptNeutral;

    const movementDurationTmp = [];

    for (let j = 0; j < numberOfImagesExceptNeutral; j++) {
      movementDurationTmp.push(imageDurationTmp);
    }
    movementDurationTmp.push(neutralPositionDuration);

    compoItem.durationList = movementDurationTmp;
  }

  return compositionTmp;
}
