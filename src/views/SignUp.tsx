import React, { FC } from 'react';
import SignUpForm from '../components/main/SignUpForm';

const SignUp: FC = () => {
  return (
    <section id="sign-up" className="main-content">
      <SignUpForm />
    </section>
  );
};

export default SignUp;
