import {
  allCompositionUrl,
  allScoreUrl,
  apiSignInUrl,
  apiSignUpUrl,
  compositionUrl,
  fetchDataUserUrl,
  scoreUrl,
} from '../config/urlConstants';
import {
  AuthData,
  CreateUserComposition,
  CreateUserScore,
  LoggedResponse,
  UserComposition,
  UserScore,
} from '../models/ApiDataInterfaces';
import { ApiResponse } from '../models/ApiResponseInterface';
import { getStoredToken } from './localStorageService';

async function fetchApi<T>(req: RequestInfo, successMessage: string, errorMessage: string): Promise<ApiResponse<T>> {
  try {
    const apiResponse = await fetch(req);
    const jsonApiResponse = await apiResponse.json();

    if (apiResponse.ok) {
      return {
        success: true,
        data: jsonApiResponse,
        message: successMessage,
      };
    } else {
      console.error(errorMessage, jsonApiResponse.message);
      return {
        success: false,
        message: errorMessage,
      };
    }
  } catch (e) {
    console.error(errorMessage, e);
    return {
      success: false,
      message: errorMessage,
    };
  }
}

export async function apiSignIn(authData: AuthData): Promise<ApiResponse<unknown>> {
  const req = new Request(apiSignInUrl, {
    method: 'POST',
    body: JSON.stringify(authData),
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    mode: 'cors',
  });

  const successMessage = 'Inscription réussie';
  const errorMessage = "Erreur d'inscrition";

  return await fetchApi(req, successMessage, errorMessage);
}

export async function apiSignUp(authData: AuthData): Promise<ApiResponse<LoggedResponse>> {
  const req = new Request(apiSignUpUrl, {
    method: 'POST',
    body: JSON.stringify(authData),
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    mode: 'cors',
  });

  const successMessage = 'Connexion réussie';
  const errorMessage = 'Erreur de connexion';

  return await fetchApi(req, successMessage, errorMessage);
}

async function loggedFetchApi<T>(
  url: string,
  method: 'GET' | 'POST' | 'PUT' | 'DELETE',
  body: unknown,
  successMessage: string,
  errorMessage: string
): Promise<ApiResponse<T>> {
  const token = getStoredToken();

  if (!token) {
    return {
      success: false,
      message: "Vous n'êtes pas connecté.e",
    };
  }

  const req = new Request(url, {
    method,
    body: body !== undefined ? JSON.stringify(body) : undefined,
    headers: new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    }),
  });

  return fetchApi(req, successMessage, errorMessage);
}

export async function apiGetUserEmail(): Promise<ApiResponse<string>> {
  const successMessage = 'Les données utilisateurice ont été correctement chargées.';
  const errorMessage = 'Erreur lors de la récupération des données utilisateurice';

  return loggedFetchApi(fetchDataUserUrl, 'GET', undefined, successMessage, errorMessage);
}

export async function apiFetchScores(): Promise<ApiResponse<UserScore[]>> {
  const successMessage = 'Les partitions a été correctement chargée';
  const errorMessage = 'Erreur lors de la récupération des partitions';

  return loggedFetchApi(allScoreUrl, 'GET', undefined, successMessage, errorMessage);
}

export async function apiSaveNewScore(score: CreateUserScore): Promise<ApiResponse<unknown>> {
  const successMessage = 'La partition a bien été enregistrée.';
  const errorMessage = "Erreur lors de l'enregistrement de la partition";

  return loggedFetchApi(scoreUrl, 'POST', score, successMessage, errorMessage);
}

export async function apiDeleteScore(scoreId: number): Promise<ApiResponse<unknown>> {
  const successMessage = 'La partition a bien été supprimée.';
  const errorMessage = 'Erreur de suppression de la partition';

  return loggedFetchApi(scoreUrl, 'DELETE', scoreId, successMessage, errorMessage);
}

export async function apiFetchCompositions(): Promise<ApiResponse<UserComposition[]>> {
  const successMessage = 'Les compositions ont été correctement chargées';
  const errorMessage = 'Erreur lors de la récupération des compositions';

  return loggedFetchApi(allCompositionUrl, 'GET', undefined, successMessage, errorMessage);
}

export async function apiSaveNewComposition(composition: CreateUserComposition): Promise<ApiResponse<unknown>> {
  const successMessage = 'La composition a bien été enregistrée.';
  const errorMessage = "Erreur lors de l'enregistrement de la composition";

  return loggedFetchApi(compositionUrl, 'POST', composition, successMessage, errorMessage);
}

export async function apiDeleteComposition(compositionId: number): Promise<ApiResponse<unknown>> {
  const successMessage = 'La composition a bien été supprimée.';
  const errorMessage = 'Erreur lors de la suppression de la composition';

  return loggedFetchApi(compositionUrl, 'DELETE', compositionId, successMessage, errorMessage);
}
