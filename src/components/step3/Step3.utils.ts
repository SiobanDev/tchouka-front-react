import { CompositionItem } from '../../models/ClientDataInterfaces';

export function getAllImageSrcList(composition: CompositionItem[]): string[] {
  const allImageSrcList: string[] = [];

  composition.forEach((compositionItem) => {
    compositionItem.movementList.forEach((noteMovementSrc) => {
      allImageSrcList.push(noteMovementSrc);
    });
  });

  return allImageSrcList;
}

export function getAllImageDurationList(composition: CompositionItem[]): number[] {
  const allImageDurationList: number[] = [];

  composition.forEach((compositionItem) => {
    compositionItem.durationList.forEach((movementDuration) => allImageDurationList.push(movementDuration));
  });

  return allImageDurationList;
}

export function getAllSoundDurationList(composition: CompositionItem[]): number[] {
  const allSoundDurationList: number[] = [];

  composition.forEach((compositionItem) => {
    const durationSum = compositionItem.durationList.reduce(function (a, b) {
      return a + b;
    }, 0);

    allSoundDurationList.push(durationSum);
  });

  return allSoundDurationList;
}

export function getAllImageDelayList(allImageDurationList: number[]): number[] {
  const allImageDelayList = [0];

  const addSumOfImageDelay = (previousSum: number, nextDuration: number): number => {
    allImageDelayList.push(previousSum + nextDuration);
    return previousSum + nextDuration;
  };

  allImageDurationList.reduce(addSumOfImageDelay, 0);

  return allImageDelayList;
}

export function getAllSoundDelayList(allSoundDurationList: number[]): number[] {
  const allSoundDelayList = [0];

  const addSumOfSoundDelay = (previousSum: number, nextDuration: number): number => {
    allSoundDelayList.push(previousSum + nextDuration);
    return previousSum + nextDuration;
  };

  allSoundDurationList.reduce(addSumOfSoundDelay, 0);

  return allSoundDelayList;
}

export function addBackgroundToImages(allImageSrcList: string[], imageCount: number): string {
  const movementImageSrc = allImageSrcList[imageCount]
    ? allImageSrcList[imageCount].replace('.svg', '-fd.svg')
    : allImageSrcList[0].replace('.svg', '-fd.svg') + '-fd.svg';

  return movementImageSrc;
}
