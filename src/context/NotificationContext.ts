import React from 'react';
//Models
import { NotificationType } from '../models/NotificationInterfaces';

interface InitialContextValuesProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  severityKind: NotificationType;
  setSeverityKind: (severityKind: NotificationType) => void;
  notificationMessage: string;
  setNotificationMessage: (notificationMessage: string) => void;
}

const initialValues: InitialContextValuesProps = {
  open: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setOpen: () => {},
  severityKind: 'error',
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setSeverityKind: () => {},
  notificationMessage: 'Une erreur est survenue.',
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setNotificationMessage: () => {},
};

const NotificationContext = React.createContext(initialValues);

export default NotificationContext;
