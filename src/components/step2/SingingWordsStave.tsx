import React, { FC } from 'react';
import { BodyPartWord } from '../../config/mediasConstants';
//Styles
import '../stave/Staves.style.scss';

interface SingingWordStaveProps {
  singingWordList: BodyPartWord[];
}

const SingingWordsStave: FC<SingingWordStaveProps> = ({ singingWordList }) => {
  return (
    <div className="word-stave">
      {singingWordList.map((singingWord, i) => (
        <div className="word-container" key={i}>
          {singingWord}
        </div>
      ))}
    </div>
  );
};

export default SingingWordsStave;
