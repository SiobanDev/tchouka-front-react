export const home = 0;
export const rythmStep = 1;
export const percussionStep = 2;
export const learningStep = 3;

export const timeCodeInterval = 50;

export const contactEmail = process.env.REACT_APP_CONTACT_EMAIL;
