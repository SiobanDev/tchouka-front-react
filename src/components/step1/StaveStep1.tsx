import React, { FC } from 'react';
//Styles
import '../stave/Staves.style.scss';
//Components
import AddedNote from '../stave/AddedNote';
import { ScoreItem } from '../../models/ClientDataInterfaces';

interface StaveStep1Props {
  id: string;
  scoreNotes: ScoreItem[];
}

const StaveStep1: FC<StaveStep1Props> = ({ id, scoreNotes }) => {
  if (scoreNotes) {
    return (
      <div id={`stave-${id}`} className="stave">
        {scoreNotes.map((note, i) => {
          return <AddedNote noteData={note} key={i} />;
        })}
      </div>
    );
  }
  return null;
};

export default StaveStep1;
