import React, { FC } from 'react';
import SignInForm from '../components/main/SignInForm';

const SignIn: FC = () => {
  return (
    <section id="sign-up" className="main-content">
      <SignInForm />
    </section>
  );
};

export default SignIn;
