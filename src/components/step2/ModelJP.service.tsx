import React from 'react';
//Constants
import { BodyPartWord, bodyPartMovementList, bodyPartSound } from '../../config/mediasConstants';
import { CompositionItem, ScoreItem } from '../../models/ClientDataInterfaces';
import { removeStoredScore } from '../../services/localStorageService';

function createComposition(
  bodyPartName: BodyPartWord,
  score: ScoreItem[],
  composition: CompositionItem[],
  setComposition: (compo: CompositionItem[]) => void
): void {
  if (composition.length < score.length) {
    removeStoredScore();

    const newCompositionItem = {
      id: composition.length,
      durationList:
        bodyPartMovementList(bodyPartName).length === 1
          ? [score[composition.length].duration]
          : [score[composition.length].duration / 2, score[composition.length].duration / 2],
      movementList: bodyPartMovementList(bodyPartName),
      singingWord: bodyPartName,
      sound: bodyPartSound(bodyPartName),
    };

    setComposition([...composition, newCompositionItem]);
  }
}

function playSoundOfBodyPart(bodyPartName: BodyPartWord, score: ScoreItem[], composition: CompositionItem[]): void {
  if (score.length !== composition.length) {
    const soundOfBodyPart = new Audio(bodyPartSound(bodyPartName));
    if (soundOfBodyPart) {
      soundOfBodyPart.volume = 0.5;
      soundOfBodyPart.addEventListener('canplaythrough', () => {
        /* the audio is now playable; play it if permissions allow */
        soundOfBodyPart.play().catch((e) => {
          console.error('Error while playing sound', e);
        });
      });
    }
  }
}

function playOneMovement(
  bodyPartName: BodyPartWord,
  setMovementImageToDisplay: (nodes: JSX.Element[]) => void,
  score: ScoreItem[],
  composition: CompositionItem[]
): void {
  if (score.length !== composition.length) {
    setMovementImageToDisplay([
      <img
        className="model animated-model"
        src={bodyPartMovementList(bodyPartName)[0]}
        alt="movement"
        key={bodyPartName}
      />,
    ]);

    if (bodyPartMovementList(bodyPartName).length > 1) {
      setTimeout(setMovementImageToDisplay, 200, [
        <img
          className="model animated-model"
          src={bodyPartMovementList(bodyPartName)[1]}
          alt="movement"
          key={bodyPartName}
        />,
      ]);
    } else {
      setTimeout(setMovementImageToDisplay, 400, []);
    }
  }
}

export function handleClickOnBodyPart(
  bodyPartName: BodyPartWord,
  score: ScoreItem[],
  composition: CompositionItem[],
  setComposition: (compo: CompositionItem[]) => void,
  setMovementImageToDisplay: (nodes: JSX.Element[]) => void
): void {
  createComposition(bodyPartName, score, composition, setComposition);
  playSoundOfBodyPart(bodyPartName, score, composition);
  playOneMovement(bodyPartName, setMovementImageToDisplay, score, composition);
}
