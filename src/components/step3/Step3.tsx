import React, { FC, useContext } from 'react';
//Components
import { step2Url } from '../../config/urlConstants';
import CompositionContext from '../../context/CompositionContext';
import PreviousStepButton from '../shared/PreviousStepButton';
import '../shared/StepButtons.style.scss';
import Controls from './animationControls/Controls';
import JPAnimation from './JPAnimation';
//Styles
import './Step3.style.scss';
//Utils
import { getAllImageDelayList, getAllImageDurationList } from './Step3.utils';

const Step3: FC = () => {
  const { composition } = useContext(CompositionContext);
  const allImageDurationList = getAllImageDurationList(composition);
  const allImageDelayList = getAllImageDelayList(allImageDurationList);

  return (
    <section id="step3" className="main-content">
      <div id="column1">
        <JPAnimation allImageDelayList={allImageDelayList} />
      </div>

      <div id="column2">
        <p className="instruction">
          <span className="round-icon">3</span>Je chante les mots associés à chaque partie du corps pour mieux mémoriser
          mon enchaînement Tchouka !
        </p>

        <Controls allImageDelayList={allImageDelayList} />
      </div>

      <div id="step-buttons-container">
        <PreviousStepButton
          handleClick={() => {
            /* no need to do extra data handling */
          }}
          previousPageUrl={step2Url}
        />
      </div>
    </section>
  );
};

export default Step3;
