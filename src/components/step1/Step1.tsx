import { faSave } from '@fortawesome/free-regular-svg-icons';
import { faBackspace, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, useContext, useEffect, useState } from 'react';
//Libraries
import Loader from 'react-loader-spinner';
import { step2Url } from '../../config/urlConstants';
import LoginContext from '../../context/LoginContext';
import NotificationContext from '../../context/NotificationContext'; //Constants
//Contexts
import ScoreContext from '../../context/ScoreContext';
//Utils
import { apiSaveNewScore } from '../../services/apiServices';
import { getStoredScore, getStoredUserId, removeStoredScore, storeScore } from '../../services/localStorageService';
import AlertModal from '../shared/AlertModal';
import InscriptionHook from '../shared/InscriptionHook';
import NextStepButton from '../shared/NextStepButton';
import ResponseIcon from '../shared/ResponseIcon';
import '../shared/StepButtons.style.scss';
//Components
import AvailableNotesContainer from './AvailableNotesContainer';
import StaveContainerStep1 from './StaveContainerStep1';
//Styles
import './Step1.style.scss';

const Step1: FC = () => {
  const { score, setScore } = useContext(ScoreContext);
  const { loggedIn } = useContext(LoginContext);
  const [waitingForApiResponse, setWaitingForApiResponse] = useState(false);
  const { setOpen, setSeverityKind, setNotificationMessage, open, notificationMessage, severityKind } = useContext(
    NotificationContext
  );

  useEffect(() => {
    if (score.length === 0) {
      const storedScored = getStoredScore();
      if (storedScored) {
        setScore(storedScored);
      }
    }
  }, [score, setScore]);

  const handleBackUp = async (): Promise<void> => {
    try {
      const userId = getStoredUserId();
      if (userId === null) {
        throw new Error('No user id in local storage');
      }

      const apiScore = {
        user: userId,
        title: `Ma super partition n°${Math.trunc(Math.random() * 10)}`,
        noteList: score,
      };
      setWaitingForApiResponse(true);
      const apiResponse = await apiSaveNewScore(apiScore);

      if (apiResponse.success) {
        setSeverityKind('success');
        setNotificationMessage(apiResponse.message);
        setOpen(true);
      } else {
        setSeverityKind('error');
        setNotificationMessage(apiResponse.message);
        setOpen(true);
      }
    } catch (e) {
      console.error('Error saving the new score : ', e);
    } finally {
      setWaitingForApiResponse(false);
    }
  };

  const handleBackspace = (): void => {
    if (score.length > 0) {
      removeStoredScore();

      const scoreTmp = [...score];
      scoreTmp.splice(score.length - 1, 1);
      setScore(scoreTmp);
    }
  };

  const handleReset = (): void => {
    removeStoredScore();
    setScore([]);
  };

  const goToNextStep = (): void => {
    storeScore(score);
  };

  return (
    <section id="step1" className="main-content">
      <AlertModal modalOpen={open} closeModal={() => setOpen(false)}>
        {notificationMessage}
        <ResponseIcon severityKind={severityKind} />
      </AlertModal>
      <p className="instruction">
        <span className="round-icon">1</span>J'écris ma partition rythmique en cliquant sur les notes ci-dessous.
      </p>
      <InscriptionHook step={1} />
      <AvailableNotesContainer />
      {loggedIn && waitingForApiResponse && <Loader type="TailSpin" color="#2ca4a0ff" height={30} width={30} />}
      <FontAwesomeIcon
        id="save-button"
        className={`edition-button hidden ${loggedIn && !waitingForApiResponse ? 'visible' : ''}`}
        icon={faSave}
        onClick={handleBackUp}
      />
      <FontAwesomeIcon className="edition-button" icon={faBackspace} onClick={handleBackspace} />
      <FontAwesomeIcon className="trash edition-button" icon={faTrash} onClick={handleReset} />

      <StaveContainerStep1 />
      <div id="step-buttons-container">
        <NextStepButton handleClick={goToNextStep} nextPageUrl={step2Url} text="Étape suivante" />
      </div>
    </section>
  );
};

export default Step1;
