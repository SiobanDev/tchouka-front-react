import React, { FC } from 'react';
import { ScoreItem } from '../../models/ClientDataInterfaces';
//Styles
import './Staves.style.scss';

interface AddedNoteProps {
  noteData: ScoreItem;
}

const AddedNote: FC<AddedNoteProps> = ({ noteData }) => {
  if (noteData) {
    return <img className="added-note" src={noteData.imageSrc} alt="added-note" />;
  }

  console.log('error in AddedNote');
  return null;
};

export default AddedNote;
