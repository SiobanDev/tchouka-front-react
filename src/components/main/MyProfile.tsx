import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faShare, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
//Libraries
import Loader from 'react-loader-spinner';
import CompositionContext from '../../context/CompositionContext';
//Context
import NotificationContext from '../../context/NotificationContext';
import ScoreContext from '../../context/ScoreContext';
import { UserComposition, UserScore } from '../../models/ApiDataInterfaces';
//Utils
import {
  apiDeleteComposition,
  apiDeleteScore,
  apiFetchCompositions,
  apiFetchScores,
  apiGetUserEmail,
} from '../../services/apiServices';
import { storeComposition, storeScore } from '../../services/localStorageService';
//Components
import AlertModal from '../shared/AlertModal';
import ResponseIcon from '../shared/ResponseIcon';
//Styles
import './MyProfile.style.scss';

const MyProfile: FC = () => {
  const [waitingForApiResponse, setWaitingForApiResponse] = React.useState(true);
  const { setOpen, setSeverityKind, setNotificationMessage, open, notificationMessage, severityKind } = useContext(
    NotificationContext
  );
  const [renderedScoreList, setRenderedScoreList] = useState<JSX.Element[]>([]);
  const [renderedCompositionList, setRenderedCompositionList] = useState<JSX.Element[]>([]);
  const apiErrorMessage = 'Erreur avec la récupération des données.';
  const [userEmail, setUserEmail] = useState<string>('');
  const { setComposition } = useContext(CompositionContext);
  const { setScore } = useContext(ScoreContext);

  const handleScoreDelete = async (scoreId: number): Promise<void> => {
    try {
      setWaitingForApiResponse(true);
      await apiDeleteScore(scoreId);
      // TODO handle api response
    } catch (e) {
      console.error('Error in MyProfile : ', e);
    } finally {
      setWaitingForApiResponse(false);
    }
  };

  const handleCompositionDelete = async (compositionId: number): Promise<void> => {
    try {
      setWaitingForApiResponse(true);
      await apiDeleteComposition(compositionId);
      // TODO handle api response
    } catch (e) {
      console.error('Error in MyProfile : ', e);
    } finally {
      setWaitingForApiResponse(false);
    }
  };

  const handleScoreCreation = useCallback(
    (createdScore: UserScore) => {
      setScore(createdScore.noteList);
      storeScore(createdScore.noteList);
      setSeverityKind('success');
      setNotificationMessage("Ma partition a été chargée à l'étape 1");
      setOpen(true);
    },
    [setScore, setSeverityKind, setNotificationMessage, setOpen]
  );

  const handleCompositionCreation = useCallback(
    (createdComposition: UserComposition) => {
      setComposition(createdComposition.movementList);
      storeComposition(createdComposition.movementList);
      setSeverityKind('success');
      setNotificationMessage("Ma composition a été chargée à l'étape 2");
      setOpen(true);
    },
    [setComposition, setSeverityKind, setNotificationMessage, setOpen]
  );

  const getUserEmail = useCallback(async () => {
    try {
      const EmailApiResponse = await apiGetUserEmail();

      if (EmailApiResponse.success) {
        setUserEmail(EmailApiResponse.data);
      }

      setWaitingForApiResponse(false);
    } catch (e) {
      setWaitingForApiResponse(false);
      console.error('Error in MyProfile : ', e);
    }
  }, []);

  const getUserCompositions = useCallback(async () => {
    try {
      const apiResponse = await apiFetchCompositions();

      if (apiResponse.success) {
        const compositionListTmp = apiResponse.data.map((composition) => (
          <li key={composition.id}>
            <div className="user-data-number round-icon">{composition.movementList.length}</div>
            <p className="composition-title">{composition.title}</p>
            <FontAwesomeIcon
              className="trash edition-button"
              icon={faTrashAlt}
              onClick={() => handleCompositionDelete(composition.id)}
            />
            <FontAwesomeIcon
              className="edition-button"
              icon={faShare}
              onClick={() => handleCompositionCreation(composition)}
            />
          </li>
        ));
        setRenderedCompositionList(compositionListTmp);
      }
      setWaitingForApiResponse(false);
    } catch (e) {
      setWaitingForApiResponse(false);
      console.error('Error in MyProfile : ', e);
    }
  }, [handleCompositionCreation]);

  const getUserScores = useCallback(async () => {
    try {
      const scoreDataApiResponse = await apiFetchScores();

      if (scoreDataApiResponse.success) {
        const scoreListTmp = scoreDataApiResponse.data.map((score) => (
          <li key={score.id}>
            <div className="user-data-number round-icon">{score.noteList.length}</div>
            <p className="score-title">{score.title}</p>
            <FontAwesomeIcon
              className="trash edition-button"
              icon={faTrash}
              onClick={() => handleScoreDelete(score.id)}
            />
            <FontAwesomeIcon className="edition-button" icon={faShare} onClick={() => handleScoreCreation(score)} />
          </li>
        ));

        setRenderedScoreList(scoreListTmp);
      }
      setWaitingForApiResponse(false);
    } catch (e) {
      setWaitingForApiResponse(false);
      console.error('Error in MyProfile : ', e);
    }
  }, [handleScoreCreation]);

  useEffect(() => {
    Promise.all([getUserEmail(), getUserScores(), getUserCompositions()]).catch((e) => {
      console.error('Error while profile data loading', e);
    });
  }, [getUserEmail, getUserScores, getUserCompositions]);

  return (
    <section id="my-profile" className="main-content">
      <AlertModal modalOpen={open} closeModal={() => setOpen(false)}>
        {notificationMessage}
        <ResponseIcon severityKind={severityKind} />
      </AlertModal>

      <div id="profile-header">
        <div className="profile-icon">T</div>
        <p className="profile-email">{userEmail}</p>
      </div>

      <div id="profile-content">
        <div className="column column1">
          <div className="user-data-header">
            <div className="user-creation-total-count">{renderedScoreList?.length}</div>
            <h4>Mes partitions TCHoUKA</h4>
            <ul className="user-data-content">
              {waitingForApiResponse ? (
                <Loader type="TailSpin" color="#2ca4a0ff" height={45} width={45} />
              ) : renderedScoreList ? (
                renderedScoreList
              ) : (
                apiErrorMessage
              )}
            </ul>
          </div>
        </div>

        <div className="column column2">
          <div className="user-data-header">
            <div className="user-creation-total-count">{renderedCompositionList?.length}</div>
            <h4>Mes compositions TCHoUKA</h4>
            <ul className="user-data-content">
              {waitingForApiResponse ? (
                <Loader type="TailSpin" color="#2ca4a0ff" height={45} width={45} />
              ) : renderedCompositionList ? (
                renderedCompositionList
              ) : (
                apiErrorMessage
              )}
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default MyProfile;
