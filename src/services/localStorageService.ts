import { CompositionItem, ScoreItem } from '../models/ClientDataInterfaces';

const tokenStorageKey = 'token';
const userIdStorageKey = 'userId';
const scoreStorageKey = 'score';
const compositionStorageKey = 'composition';

function getStoredData<T>(storageKey: string): T | null {
  const stored = localStorage.getItem(storageKey);

  if (stored === null) {
    return null;
  }

  return JSON.parse(stored);
}

export function storeScore(score: ScoreItem[]): void {
  localStorage.setItem(scoreStorageKey, JSON.stringify(score));
}

export function getStoredScore(): ScoreItem[] | null {
  return getStoredData(scoreStorageKey);
}

export function removeStoredScore(): void {
  localStorage.removeItem(scoreStorageKey);
}

export function storeComposition(composition: CompositionItem[]): void {
  localStorage.setItem(compositionStorageKey, JSON.stringify(composition));
}

export function getStoredComposition(): CompositionItem[] | null {
  return getStoredData(compositionStorageKey);
}

export function removeStoredComposition(): void {
  localStorage.removeItem(compositionStorageKey);
}

export function storeToken(token: string): void {
  localStorage.setItem(tokenStorageKey, token);
}

export function getStoredToken(): string | null {
  return localStorage.getItem(tokenStorageKey);
}

export function removeStoredToken(): void {
  return localStorage.removeItem(tokenStorageKey);
}

export function storeUserId(userId: number): void {
  localStorage.setItem(userIdStorageKey, userId.toString());
}

export function getStoredUserId(): number | null {
  return getStoredData(userIdStorageKey);
}

export function removeStoredUserId(): void {
  return localStorage.removeItem(userIdStorageKey);
}
