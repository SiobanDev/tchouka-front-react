import React, { FC, useContext, useState } from 'react';
//Assets
import { bodyPartList, jpNeutre, jpNeutreTransp } from '../../config/mediasConstants';
import CompositionContext from '../../context/CompositionContext';
//Contexts
import ScoreContext from '../../context/ScoreContext';
//Components
import BodyPart from './BodyPart';
//Utils
import { handleClickOnBodyPart } from './ModelJP.service';
//Styles
import './ModelJP.style.scss';

const ModelJP: FC = () => {
  const { score } = useContext(ScoreContext);
  const { composition, setComposition } = useContext(CompositionContext);
  const [movementImageToDisplay, setMovementImageToDisplay] = useState<JSX.Element[]>([]);

  return (
    <div className="model-container">
      {movementImageToDisplay.length > 0 ? (
        movementImageToDisplay
      ) : (
        <img className="model model-transp" src={jpNeutreTransp} alt="neutral-model" />
      )}
      <img className="model neutral-model" src={jpNeutre} alt="neutral-model" />
      {bodyPartList.map((bodyPart, i) => (
        <BodyPart
          name={bodyPart}
          handleClick={() => {
            handleClickOnBodyPart(bodyPart, score, composition, setComposition, setMovementImageToDisplay);
          }}
          key={i}
        />
      ))}
    </div>
  );
};

export default ModelJP;
